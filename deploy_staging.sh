#!/bin/bash

echo ' '
echo '####### Start of deploying #######'
echo ' '

npm i @angular/cli -g

npm i

ng -v

echo '####### Angular was installed #######'

ng build -prod

apt-get update && apt-get install rsync -y

rsync -avz ./dist $CI_USER_WWW@$CI_HOST:$CI_PATH --exclude '.ssh' --cvs-exclude

echo ' '
echo '####### The project was deployed succesfully #######'
echo ' '

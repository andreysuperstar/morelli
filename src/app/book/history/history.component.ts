import { Component, OnInit } from '@angular/core';

import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  carousel: NgxCarousel;

  mp4 = 'https://www.w3schools.com/html/mov_bbb.mp4';
  ogg = 'https://www.w3schools.com/html/mov_bbb.ogg';

  constructor() { }

  ngOnInit() {

    this.carousel = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 1,
      speed: 400,
      interval: 4000,
      point: {visible: true},
      load: 2,
      loop: true,
      touch: true
    };

  }

}

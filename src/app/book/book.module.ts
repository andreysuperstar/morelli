import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';

import { BookRoutingModule } from './book-routing.module';

import { SharedModule } from '../shared/shared.module';

import { BookComponent } from './book.component';
import { HistoryComponent } from './history/history.component';

@NgModule({
  imports: [
    CommonModule,
    BookRoutingModule,
    NgxCarouselModule,
    SharedModule
  ],
  declarations: [
    BookComponent,
    HistoryComponent
  ]
})
export class BookModule { }

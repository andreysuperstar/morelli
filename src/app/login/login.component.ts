import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../core/auth.service';

import { Login } from './shared/login.interface';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  type = 'password';
  error: string;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

  private build() {
    this.form = this.fb.group({
      login: [
        null,
        {
          validators: [
            Validators.required,
            // Validators.pattern(/^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$/)
            Validators.pattern(/^(?:[A-Z\d][A-Z\d_ -]*|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i)
          ],
          // asyncValidators: [],
          updateOn: 'change'
        }
      ],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(4)
        ]
      ]
    });
  }

  ngOnInit() {
    this.build();
  }

  toggle() {
    this.type = this.type === 'password' ? 'text' : 'password';
  }

  submit({ value, valid }: { value: Login, valid: boolean }) {
    console.log('LoginComponent#submit value', value, 'valid', valid);
    // console.log('LoginComponent#submit value', this.form.value, 'valid', this.form.valid);

    if (!value.login) {
      this.form.markAsTouched();
      this.error = 'Введите логин и пароль';
      return;
    }

    this.auth
      .login(value)
      .subscribe(() => {
        console.log('LoginComponent#submit response');

        if (this.auth.logged.getValue()) {
          const redirect = this.auth.redirect ? this.auth.redirect : '/';

          this.router.navigate([redirect]);
        } else {
          this.error = 'Неверные логин или пароль';
        }
      });
  }

  reset() {
    if (this.error) {
      this.error = null;
    }
  }

  ngOnDestroy() {
    if (this.auth.redirect) {
      this.auth.redirect = null;
    }
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Link } from '../shared';

@Injectable()
export class LinksService {

  private new: Link[];
  private recent: Link[];

  constructor(private http: HttpClient) {

    this.new = [
      { id: 1, text: 'Революционная раздвижная система «Luxury Wood»' },
      { id: 2, text: 'Дверные ручки серии «Elena» теперь в продаже' },
      { id: 3, text: 'Классическая раздвижная система для деревянных дверей SLIDING' },
      { id: 4, text: 'Инновационная раздвижная система Invisible со скрытым профилем' }
    ];

    this.recent = [
      { id: 1, text: 'Монтаж раздвижных систем в загородном доме' },
      { id: 2, text: 'Серия ручек и замков для входных дверей' },
      { id: 3, text: 'Нюансы и тонкости производства дверных петель' }
    ];

  }

  get new$(): Observable<Link[]> {
    return of(this.new);

    // return this.http.get<Link[]>('/links');
  }

  get recent$(): Observable<Link[]> {
    return of(this.recent);

    // return this.http.get<Link[]>('/links');
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { share } from 'rxjs/operators/share';
import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators/delay';
import { tap } from 'rxjs/operators/tap';

import { environment } from '../../environments/environment';

import { NavbarComponent } from '../navbar/navbar.component';

import { Login } from '../login/shared/login.interface';

@Injectable()
export class AuthService {

  logged = new BehaviorSubject<boolean>(false);
  logged$: Observable<boolean> = this.logged
    .asObservable()
    .pipe(share());
  redirect: string;

  constructor(private http: HttpClient) {

    if (environment.log) {
      this.logged.next(true);
    }

  }

  login(credentials: Login): Observable<any> {
    return of(null).pipe(
      delay(1000),
      tap(() => {
        console.log(credentials.login);
        if ((credentials.login === 'test' || credentials.login === 'test@test.com') && credentials.password === 'test') {
          this.logged.next(true);
        }
      })
    );

    // return this.http
    //   .get('/login')
    //   .pipe(tap((response: boolean) => this.logged.next(response)));
  }

  logout() {
    console.log('AuthService#logout');
    this.logged.next(false);
  }

}

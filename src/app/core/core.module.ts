import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AuthService } from './auth.service';
import { LinksService } from './links.service';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    AuthService,
    LinksService
  ]
})
export class CoreModule { }

import { Component, OnInit, AfterContentInit, ContentChildren, QueryList, Output, EventEmitter } from '@angular/core';

import { TabComponent } from '../tab/tab.component';

import { Tab } from '../tab.interface';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterContentInit {

  constructor() { }

  @ContentChildren(TabComponent) tabs: QueryList<Tab>;

  @Output() selected: EventEmitter<Tab> = new EventEmitter(true);

  ngOnInit() {
  }

  ngAfterContentInit() {
    const active = this.tabs.filter((tab) => tab.active);

    if (active.length === 0) {
      this.select(this.tabs.first);
    }
  }

  private select(tab: Tab) {

    if (!tab.link) {
      this.tabs.forEach(tab => tab.active = false);

      tab.active = true;

      this.selected.emit(Object.assign(tab, { index: this.tabs.toArray().indexOf(tab) }));
    }

  }

}

import { Component, OnInit, Input} from '@angular/core';

import { Tab } from '../tab.interface';

@Component({
  selector: 'tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements Tab, OnInit {

  constructor() { }

  @Input() heading: string;
  @Input() active = false;
  @Input() link: string[];

  ngOnInit() {
  }

}

export interface Tab {
  heading: string;
  link?: string[];
  active: boolean;
  index?: number;
}

import { Directive, OnInit, Renderer2, ElementRef, HostBinding, HostListener, OnDestroy } from '@angular/core';

@Directive({
  selector: '[modal]'
})
export class ModalDirective implements OnInit, OnDestroy {

  private backdrop: HTMLDivElement;

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  @HostBinding() private hidden = true;
  @HostBinding('class.visibility') private visibility = false;

  ngOnInit() {
  }

  private overlay() {
    this.backdrop = this.renderer.createElement('div');

    this.renderer.addClass(this.backdrop, 'backdrop');
    this.renderer.appendChild(document.body, this.backdrop);

    this.renderer.addClass(document.body, 'overlay');

    setTimeout(() => this.renderer.addClass(this.backdrop, 'show'), 0);
  }

  open() {
    this.overlay();

    this.hidden = false;

    setTimeout(() => this.visibility = true, 0);
  }

  private lighten() {
    this.renderer.removeClass(this.backdrop, 'show');

    setTimeout(() => {
      this.renderer.removeClass(document.body, 'overlay');
      this.renderer.removeChild(document.body, this.backdrop);
    }, 150);
  }

  close() {
    this.visibility = false;

    setTimeout(() => this.hidden = true, 300);

    setTimeout(this.lighten.bind(this), 150);
  }

  @HostListener('click', ['$event'])
  private hide() {
    if (event.target === this.el.nativeElement) {
      this.close();
    }
  }

  ngOnDestroy() {
    if (!this.hidden) {
      this.visibility = null;
      this.hidden = true;
      this.lighten();
    }
  }

}

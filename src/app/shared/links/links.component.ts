import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LinksService } from '../../core/links.service';

import { Link } from '../';

@Component({
  selector: 'links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {

  new$: Observable<Link[]>;
  recent$: Observable<Link[]>;

  constructor(private links: LinksService) { }

  new() {
    this.new$ = this.links.new$;
  }

  recent() {
    this.recent$ = this.links.recent$;
  }

  ngOnInit() {
    this.new();

    this.recent();
  }

}

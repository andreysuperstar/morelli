export interface Link {
  id: number;
  text: string;
}

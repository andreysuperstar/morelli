import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ModalDirective } from './modal.directive';

import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { LinksComponent } from './links/links.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tab/tab.component';
import { PlayerComponent } from './player/player.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    ModalDirective,
    LinksComponent,
    BreadcrumbsComponent,
    BreadcrumbComponent,
    TabsComponent,
    TabComponent,
    PlayerComponent
  ],
  exports: [
    ModalDirective,
    BreadcrumbsComponent,
    BreadcrumbComponent,
    LinksComponent,
    TabsComponent,
    TabComponent,
    PlayerComponent
  ]
})
export class SharedModule { }

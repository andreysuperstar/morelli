import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  playing: boolean;

  constructor() { }

  @Input() mp4: string;
  @Input() ogg: string;

  @ViewChild('video', {read: ElementRef}) video: ElementRef;

  ngOnInit() {
  }

  playback() {

    if (this.video.nativeElement.paused) {
      this.video.nativeElement.play();
    } else {
      this.video.nativeElement.pause();
    }

  }

}

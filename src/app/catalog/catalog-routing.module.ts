import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogComponent } from './catalog.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent
  },
  {
    path: 'handle',
    loadChildren: 'app/product/product.module#ProductModule',
    data: {type: 'handle'}
  },
  {
    path: 'engineering',
    loadChildren: 'app/product/product.module#ProductModule',
    data: {type: 'engineering'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions, PreloadingStrategy, PreloadAllModules } from '@angular/router';

import { AuthGuard } from './auth.guard';

import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/home/home.module#HomeModule'
  },
  {
    path: 'base',
    loadChildren: 'app/base/base.module#BaseModule'
  },
  {
    path: 'catalog',
    loadChildren: 'app/catalog/catalog.module#CatalogModule'
  },
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'user',
        loadChildren: 'app/user/user.module#UserModule'
      },
      // {
      //   path: 'admin',
      //   loadChildren: 'app/admin/admin.module#AdminModule',
      //   canLoad: [AdminGuard],
      //   canActivateChild: [AdminGuard]
      // }
    ]
  },
  { path: '**', component: NotFoundComponent }
];

const config: ExtraOptions = {
  preloadingStrategy: PreloadAllModules
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }

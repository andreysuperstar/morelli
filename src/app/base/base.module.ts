import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseRoutingModule } from './base-routing.module';

import { SharedModule } from '../shared/shared.module';

import { BaseComponent } from './base.component';
import { ProductionComponent } from './shared/production/production.component';

@NgModule({
  imports: [
    CommonModule,
    BaseRoutingModule,
    SharedModule
  ],
  declarations: [
    BaseComponent,
    ProductionComponent
  ]
})
export class BaseModule { }

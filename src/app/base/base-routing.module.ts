import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaseComponent } from './base.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent
  },
  {
    path: 'book',
    loadChildren: 'app/book/book.module#BookModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule { }

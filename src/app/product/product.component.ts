import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Type } from './shared/type.enum';
import { Category } from './shared/category.enum';

import { Specification } from './shared/specification.interface';
import { Instruction } from './shared/instruction.interface';
import { Post } from './shared/post.interface';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  type: number | any;
  category: string;

  name: string;
  image: string;

  description: string;
  specification: Specification;
  instruction: Instruction;
  comments: Post[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    const type = this.route.snapshot.data.type;

    this.type = Type[type];
    this.category = Category[this.type];

    const name = this.route.snapshot.paramMap.get('name');

    this.name = name.replace('-', ' ');

    this.description = `
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
      </p>

      <p>
        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
      </p>
    `;

    this.specification = {
      country: 'Италия',
      material: 'ЦАМ',
      composition: '(сплав, содержащий цинк, алюминий и медь) + многослойное гальваническое покрытие',
      color: 'Кофе',
      quantity: 2,
      availability: true,
      image: 'assets/dev/dimensions.png'
    };

    this.instruction = {
      video: {
        mp4: 'https://www.w3schools.com/html/mov_bbb.mp4',
        ogg: 'https://www.w3schools.com/html/mov_bbb.ogg'
      },
      steps: [
        'нужно сделать то-то и то-то',
        'нужно сделать то-то и то-то',
        'нужно сделать то-то и то-то',
        'нужно сделать то-то и то-то',
        'нужно сделать то-то и то-то'
      ]
    };

    this.comments = [
      {
        avatar: 'assets/dev/comment-avatar-1.jpg',
        name: 'Геннадий Жук',
        date: '12 января 2017',
        time: '18:10',
        heading: 'Замена ручек',
        message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
        like: 3,
        dislike: 4
      },
      {
        avatar: 'assets/dev/comment-avatar-2.jpg',
        name: 'Вера Петровна',
        date: '7 января 2018',
        time: '08:55',
        heading: 'Вопрос по комплектации',
        message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
        like: 15,
        dislike: 7
      },
      {
        avatar: 'assets/dev/comment-avatar-3.jpg',
        name: 'Варвара Чин',
        date: '25 октября 2017',
        time: '13:30',
        heading: 'Защёлки',
        message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
        like: 5,
        dislike: 1
      }
    ];

  }

  view(image: number) {}

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NgxCarouselModule } from 'ngx-carousel';

import { HandleRoutingModule } from './product-routing.module';

import { SharedModule } from '../shared/shared.module';

import { ProductComponent } from './product.component';
import { DescriptionComponent } from './shared/description/description.component';
import { DesignComponent } from './shared/design/design.component';
import { SpecificationComponent } from './shared/specification/specification.component';
import { SpecificationListComponent } from './shared/specification-list/specification-list.component';
import { InstallationComponent } from './shared/installation/installation.component';
import { CommentsComponent } from './shared/comments/comments.component';
import { CommentComponent } from './shared/comment/comment.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HandleRoutingModule,
    NgxCarouselModule,
    SharedModule
  ],
  declarations: [
    ProductComponent,
    DescriptionComponent,
    DesignComponent,
    SpecificationComponent,
    SpecificationListComponent,
    InstallationComponent,
    CommentsComponent,
    CommentComponent
  ]
})
export class ProductModule { }

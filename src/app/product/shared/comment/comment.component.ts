import { Component, OnInit, Input } from '@angular/core';

import { Post } from '../post.interface';

@Component({
  selector: 'comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  constructor() { }

  @Input() comment: Post;

  ngOnInit() {
  }

}

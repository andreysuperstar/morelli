export interface Specification {
  country: string;
  material: string;
  composition: string;
  color: string;
  quantity: number;
  availability: boolean;
  image: string;
}

import { Comment } from './comment.interface';

export interface Post extends Comment {
  avatar: string;
  name: string;
  date: string;
  time: string;
  like: number;
  dislike: number;
}

import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  constructor() { }

  @Input() description: string;

  ngOnInit() {
  }

}

import { Component, OnInit, Input } from '@angular/core';

import { Specification } from '../specification.interface';

@Component({
  selector: 'specification-list',
  templateUrl: './specification-list.component.html',
  styleUrls: ['./specification-list.component.scss']
})
export class SpecificationListComponent implements OnInit {

  constructor() { }

  @Input() specification: Specification;

  ngOnInit() {
  }

}

export interface Comment {
  heading: string;
  message: string;
}

import { Component, OnInit, Input } from '@angular/core';

import { Instruction } from '../instruction.interface';

@Component({
  selector: 'installation',
  templateUrl: './installation.component.html',
  styleUrls: ['./installation.component.scss']
})
export class InstallationComponent implements OnInit {

  constructor() { }

  @Input() instruction: Instruction;

  ngOnInit() {
  }

}

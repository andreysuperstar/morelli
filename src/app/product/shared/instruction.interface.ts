export interface Instruction {
  video: {
    mp4: string;
    ogg: string;
  };
  steps: string[];
}

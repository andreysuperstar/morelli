import { Component, OnInit, Input } from '@angular/core';

import { Specification } from '../specification.interface';

@Component({
  selector: 'specification',
  templateUrl: './specification.component.html',
  styleUrls: ['./specification.component.scss']
})
export class SpecificationComponent implements OnInit {

  constructor() { }

  @Input() specification: Specification;

  ngOnInit() {
  }

}

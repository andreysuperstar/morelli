import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Comment } from '../comment.interface';
import { Post } from '../post.interface';

@Component({
  selector: 'comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  @Input() comments: Post[];

  private build() {
    this.form = this.fb.group(
      {
        heading: [
          null,
          {
            validators: Validators.required,
            updateOn: 'change'
          }
        ],
        message: [
          null,
          {
            validators: [
              Validators.required,
              Validators.maxLength(240)
            ],
            updateOn: 'change'
          }
        ]
      }
    );
  }

  ngOnInit() {
    this.build();
  }

  submit({ value, valid }: { value: Comment, valid: boolean }) {
    if (!value.heading || !value.message) {
      this.form.markAsTouched();
      return;
    }

    console.log('CommentsComponent#submit value', value, 'valid', valid);
  }

  reset() {
    this.form.reset();
  }

  date() {
    console.log('CommentsComponent#date');
  }

  popular() {
    console.log('CommentsComponent#popular');
  }

}

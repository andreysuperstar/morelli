import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../core/auth.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  logged: boolean;
  avatar: string;
  name: string;
  progress: number;
  total: number;
  bar: number;
  score: number;
  notifications: number;
  dropdown = false;

  constructor(private auth: AuthService, private router: Router) { }

  @ViewChild('settings') settings: ElementRef;

  content() {

    this.auth
      .logged$
      .subscribe(logged => {
        this.logged = logged;
      });

    this.avatar = 'assets/dev/navbar-avatar.jpg';
    this.name = 'Morelli';
    this.score = 655;
    this.progress = 2245;
    this.total = 4500;
    this.bar = this.progress / this.total * 100;
    this.notifications = 3;

  }

  ngOnInit() {
    this.content();
  }

  @HostListener('document:click', ['$event'])
  private hide(event) {

    if (this.logged) {

      if (event.target === this.settings.nativeElement || this.dropdown) {
          this.dropdown = !this.dropdown;
      }

    }

  }

  logout() {
    this.dropdown = false;

    this.auth.logout();
  }

}

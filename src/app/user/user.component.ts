import { Component, OnInit } from '@angular/core';

import { Tab } from '../shared/tab.interface';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  collections: boolean;

  constructor() { }

  ngOnInit() {
  }

  set selected(event: Tab) {
    this.collections = event.index === 1 ? true : false;
  }

}

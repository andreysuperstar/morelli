import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';

import { SharedModule } from '../shared/shared.module';

import { UserComponent } from './user.component';
import { StatisticsComponent } from './shared/statistics/statistics.component';
import { ProfileComponent } from './shared/profile/profile.component';
import { CollectedComponent } from './shared/collected/collected.component';
import { CollectionModalComponent } from './shared/collection-modal/collection-modal.component';
import { CollectionsComponent } from './shared/collections/collections.component';
import { ShipSettingsModalComponent } from './shared/ship-settings-modal/ship-settings-modal.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  declarations: [
    UserComponent,
    StatisticsComponent,
    ProfileComponent,
    CollectedComponent,
    CollectionModalComponent,
    CollectionsComponent,
    ShipSettingsModalComponent
  ],
  exports: [CollectedComponent]
})
export class UserModule { }

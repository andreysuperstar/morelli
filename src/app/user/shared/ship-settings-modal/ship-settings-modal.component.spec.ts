import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipSettingsModalComponent } from './ship-settings-modal.component';

describe('ShipSettingsModalComponent', () => {
  let component: ShipSettingsModalComponent;
  let fixture: ComponentFixture<ShipSettingsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipSettingsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipSettingsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';

import { ModalDirective } from '../../../shared/modal.directive';

@Component({
  selector: 'ship-settings-modal',
  templateUrl: './ship-settings-modal.component.html',
  styleUrls: ['./ship-settings-modal.component.scss']
})
export class ShipSettingsModalComponent implements OnInit {

  constructor(private modal: ModalDirective) { }

  @ViewChild('dismiss', {read: ElementRef}) private dismiss: ElementRef;

  ngOnInit() {
  }

  open() {
    this.modal.open();
  }

  @HostListener('click', ['$event'])
  private hide() {
    if (event.target === this.dismiss.nativeElement) {
      this.modal.close();
    }
  }

}

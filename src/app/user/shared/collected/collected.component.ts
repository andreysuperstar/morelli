import { Component, OnInit, ViewChild } from '@angular/core';

import { CollectionModalComponent } from '../collection-modal/collection-modal.component';

@Component({
  selector: 'collected',
  templateUrl: './collected.component.html',
  styleUrls: ['./collected.component.scss']
})
export class CollectedComponent implements OnInit {

  constructor() { }

  @ViewChild(CollectionModalComponent) private modal: CollectionModalComponent;

  ngOnInit() {
  }

  open() {
    this.modal.open();
  }

}

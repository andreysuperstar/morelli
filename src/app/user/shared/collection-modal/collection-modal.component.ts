import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';

import { ModalDirective } from '../../../shared/modal.directive';

@Component({
  selector: 'collection-modal',
  templateUrl: './collection-modal.component.html',
  styleUrls: ['./collection-modal.component.scss']
})
export class CollectionModalComponent implements OnInit {

  constructor(private modal: ModalDirective) { }

  @ViewChild('dismiss', {read: ElementRef}) private dismiss: ElementRef;

  ngOnInit() {
  }

  open() {
    this.modal.open();
  }

  @HostListener('click', ['$event'])
  private hide() {
    if (event.target === this.dismiss.nativeElement) {
      this.modal.close();
    }
  }

}

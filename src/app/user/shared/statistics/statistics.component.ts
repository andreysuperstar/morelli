import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
// import { switchMap } from 'rxjs/operator/switchMap';

@Component({
  selector: 'statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  name: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {

    this.name = this.route.snapshot.parent.paramMap.get('name');

    // this.user$ = this.service.user(id);

    // this.user$ = this.route
    //   .paramMap
    //   .pipe(switchMap((params: ParamMap) => this.service.user(params.get('id'))));

  }

}

import { Component, OnInit, ViewChild } from '@angular/core';

import { ShipSettingsModalComponent } from '../ship-settings-modal/ship-settings-modal.component';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor() { }

  @ViewChild(ShipSettingsModalComponent) private modal: ShipSettingsModalComponent;

  ngOnInit() {
  }

  open() {
    this.modal.open();
  }

}
